function addTokens(input, tokens){

    if(input.length < 6) throw new Error('Input should have at least 6 characters');
    if(!(typeof input === 'string')) throw new TypeError('Invalid input');
    

    let pattern = /{([A-Z]|[a-z]){3,}:\s([A-Z]|[a-z]|\s|[0-9]){4,}}/;
    
    
    for (let i=0; i < tokens.length; i++){
       
        if(pattern.test(tokens[i])== false){

            throw new Error('Invalid array format');
        }
    }

    if(input.indexOf('...')== -1){
        return input;
    }

   else {

   let newinput = input;

    let i=0;
    while(i < tokens.length){ 
    let replacedword = tokens[i].substring(tokens[i].indexOf(": ")+2,tokens[i].length-1)
       newinput = input.replace('...', replacedword);
       input = newinput;
       i++;
    }

    return newinput;

   }

    
    
}

const app = {
    addTokens: addTokens
}
module.exports = app;


let array = ['{Subsemnatul: Tololoi Maria}','{Domiciliu: Bucuresti}', '{CNP: 12344656}'];
console.log(addTokens('Subsemnatul ... domiciliat in ... avand CNP ... .', array));
console.log(addTokens("Subsemnatul Tololoi Maria", array));

